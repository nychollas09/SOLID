package solid.enums;

import solid.repository.DezOuVintePorCento;
import solid.repository.QuinzeOuVintePorCento;
import solid.repository.RegraDeCalculo;

public enum Cargo {
    ADMINISTRADOR(new DezOuVintePorCento()),
    COORDENADOR(new DezOuVintePorCento()),
    SECRETARIO(new QuinzeOuVintePorCento()),
    FUNCIONARIO(new QuinzeOuVintePorCento());
    
    private RegraDeCalculo regra;
    
    Cargo(RegraDeCalculo regra){
        this.regra = regra;
    }
    
    public RegraDeCalculo getRegra(){
        return this.regra;
    }    
}

package solid.domain;

import java.util.Date;
import solid.enums.Cargo;

public class Funcionario {
    
    public Funcionario(){}
    
    public Funcionario(String nome, Float salario, Date dataAdmissao, Cargo cargo){
        this.nome = nome;
        this.salario = salario;
        this.dataAdmissao = dataAdmissao;
        this.cargo = cargo;
    }
    
    private String nome;
    
    private Float salario;
            
    private Date dataAdmissao;
    
    private Cargo cargo;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Float getSalario() {
        return salario;
    }

    public void setSalario(Float salario) {
        this.salario = salario;
    }

    public Date getDataAdmissao() {
        return dataAdmissao;
    }

    public void setDataAdmissao(Date dataAdmissao) {
        this.dataAdmissao = dataAdmissao;
    }

    public Cargo getCargo() {
        return cargo;
    }

    public void setCargo(Cargo cargo) {
        this.cargo = cargo;
    }    
    
    public Float calcularSalario(){
        return cargo.getRegra().calcular(this);
    }
    
}

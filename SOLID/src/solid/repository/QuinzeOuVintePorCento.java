package solid.repository;

import solid.domain.Funcionario;

public class QuinzeOuVintePorCento implements RegraDeCalculo{
    @Override
    public Float calcular(Funcionario funcionario){
        if(funcionario.getSalario() > 2000.0){
            return funcionario.getSalario() * 0.75F;
        }else{
            return funcionario.getSalario() * 0.85F;
        }
    }
}

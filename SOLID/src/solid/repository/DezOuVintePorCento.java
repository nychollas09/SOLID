package solid.repository;

import solid.domain.Funcionario;

public class DezOuVintePorCento implements RegraDeCalculo{
    @Override
    public Float calcular(Funcionario funcionario){
        if(funcionario.getSalario() > 3000.0){
            return funcionario.getSalario() * 0.8F;
        }else{
            return funcionario.getSalario() * 0.9F;
        }
    }
}

package solid.repository;

import solid.domain.Funcionario;

public interface RegraDeCalculo {
    public Float calcular(Funcionario funcionario);
}

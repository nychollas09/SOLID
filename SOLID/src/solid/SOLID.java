package solid;

import java.util.Date;
import solid.domain.Funcionario;
import solid.enums.Cargo;

public class SOLID {

    public static void main(String[] args) {
        Funcionario administrador = new Funcionario("Nichollas", 3000F, new Date(), Cargo.ADMINISTRADOR);
        Funcionario coordenador = new Funcionario("João", 2001F, new Date(), Cargo.COORDENADOR);
        
        System.out.println("Salário do administrador: " + administrador.calcularSalario());
        System.out.println("Salário do coordenador: " + coordenador.calcularSalario());
    }
    
}
